//
// @author wesen
// @copyright 2017-2019 wesen <wesen-ac@web.de>
//

exec "scripts/ac-gema-mode/gema-mode-state/environment-checker.cfg";

//
// Gema mode state updater module
// Provides methods to de-/activate the gema mode and handles automatic de-/activation of the gema mode.
// Also provides methods to fetch the current gema mode state.
//
// Requires:
//   * scripts/ac-gema-mode/notification-printer.cfg
//   * scripts/ac-gema-mode/strings.cfg
//   * scripts/ac-gema-mode/gema-mode-state/environment-checker.cfg
//

// Options

//
// Option to automatically activate the gema mode on gema maps
//
// @var int $optionAutoActivateGemaMode
//
check2init optionAutoActivateGemaMode 0;


persistidents 0;

//
// Stores whether the gema mode is currently active
//
// @var int $gemaModeIsActive
//
check2init gemaModeIsActive 0;


// Public Functions

//
// Returns whether the gema mode is currently active.
//
// @return int 1 if the gema mode is active, 0 otherwise
//
const isGemaModeActive [
  return (&& $gemaModeIsActive (isGema));
]

//
// Enables the gema mode.
//
const enableGemaMode [
  setGemaModeState 1;
]

//
// Disables the gema mode.
//
const disableGemaMode [
  setGemaModeState 0;
]


// Private Functions

//
// Sets the gema mode state (Enables or disables the gema mode).
//
// @param int $arg1 The state to which the gema mode will be set
//
const setGemaModeState [

  newGemaModeState = $arg1;
  gemaModeStateCanBeChanged = (!= $gemaModeIsActive $newGemaModeState);

  if (&& $gemaModeStateCanBeChanged (&& (= $newGemaModeState 1) (! (isGema)))) [
    // The gema mode shall be activated and the environment is not gema compatible
    printGemaNotification (format $messageGemaModeCanNotBeActivated (getEnvironmentNotGemaCompatibleReason));
    gemaModeStateCanBeChanged = 0;
  ]

  if ($gemaModeStateCanBeChanged) [

    gemaModeIsActive = $newGemaModeState;
    if ($gemaModeIsActive) [
      onEnableGemaMode;
    ][
      onDisableGemaMode;
    ]

  ]

]

//
// Automatically enables the gema mode if required.
//
const autoEnableGemaMode [

  if (&& (! $gemaModeIsActive) $optionAutoActivateGemaMode) [
    enableGemaMode;
    if (isGemaModeActive) [
      printGemaNotification $messageGemaModeAutoActivated;
    ]
  ]

]

//
// Automatically disables the gema mode if required.
//
const autoDisableGemaMode [

  if ($gemaModeIsActive) [
    disableGemaMode;
    printGemaNotification (format $messageGemaModeAutoDeactivated (getEnvironmentNotGemaCompatibleReason));
  ]

]


// Custom Events

//
// Event handler that is called when the gema mode state changes to disabled.
//
checkinit onDisableGemaMode [
]

//
// Event handler that is called when the gema mode state changes to enabled.
//
checkinit onEnableGemaMode [
]

//
// Event handler that is called after the gema mode auto de-/activation checks on map start are complete.
//
checkinit onGemaModeStateMapStartCheckAfter [
]


// Event Handlers

//
// Event handler that is called when a new map starts.
//
checkinit mapstartalways [

  if (isGema) [
    autoEnableGemaMode;
  ][
    autoDisableGemaMode;
  ]

  onGemaModeStateMapStartCheckAfter;

]

//
// Event handler that is called when the player uses /newmap.
//
checkinit onNewMap [
  disableGemaMode;
]

//
// Event handler that is called when the game is closed.
//
checkinit onQuit [
  disableGemaMode;
]
