//
// @author wesen
// @copyright 2017-2019 wesen <wesen-ac@web.de>
//

//
// Map record printer module for ac-gema-mode
//
// Handles printing of map records in the console and the menus
//
// Requires:
//   * scripts/ac-gema-mode/strings.cfg
//   * scripts/ac-gema-mode/map-record/map-record.cfg
//   * scripts/ac-gema-mode/util/time-parser.cfg
//   * scripts/ac-gema-mode/util/weapon-name-fetcher.cfg
//

// Options

//
// Option to show the player name in the console score string
//
// @var int $optionAutoActivateGemaMode
//
checkinit optionShowNameOnScore 0;

//
// Option to show the score weapon in the console score string
// Also toggles showing the score weapon in the best map record menu string
//
// @var int $optionShowScoreWeapon
//
check2init optionShowScoreWeapon 1;

//
// Option to set the comparison value for score times
//
// 0: Best map record
// 1: Best score weapon record
//
check2init optionScoreTimeCompareValue 1;


persistidents 0;

// Strings for the menus

//
// Returns a record string for the menus.
//
// @param String $arg1 The map name
// @param int $arg2 The weapon id
//
// @return String The map record menu string
//
const getMapRecordMenuString [

  mapRecordTime = (loadMapRecord $arg1 $arg2);

  if (! $mapRecordTime)[
    return $messageNoMapRecord;
  ][
    return (format $messageMapRecordTime (generateTimeString $mapRecordTime 1));
  ]

]


// Output strings for the console

//
// Generates and returns the map record output string for when the player scores.
//
// @param int $arg1 The record time in milliseconds
// @param int $arg2 The weapon id of the weapon that the player used to finish the map
//
// @return String The map record output string for when the player scores
//
const getMapRecordScoreString [

  recordMilliseconds = $arg1;
  weaponId = $arg2;

  if ($optionShowNameOnScore) [
    scoreName = (format $messageScoreNamePlayerName (player1 name));
  ][
    scoreName = $messageScoreNameYou;
  ]

  mapRecordString = (format $messageMapRecordScore $scoreName (generateTimeString $recordMilliseconds 1));

  if ($optionShowScoreWeapon) [
    mapRecordString = (concat $mapRecordString (format $messageMapRecordScoreWeapon (getWeaponName $weaponId)));
  ]

  return $mapRecordString;

]

//
// Returns the record status string (new record, tied record, no new record).
// Must be called before the new record was saved.
//
// @param String $arg1 The map name
// @param int $arg2 The record time in milliseconds
// @param int $arg3 The weapon id of the weapon that the player used to finish the map
//
// @return String The record status string
//
const getMapRecordStatusString [

  status_mapName = $arg1;
  status_recordTime = $arg2;
  status_weaponId = $arg3;

  bestMapRecordTime = (loadMapRecord $status_mapName (getBestMapRecordWeaponId $status_mapName));

  if (|| (! $bestMapRecordTime) (< $status_recordTime $bestMapRecordTime)) [
    return $messageFasterThanBestMapRecord;
  ][
    if (= $optionScoreTimeCompareValue 0) [
      return (getBestMapRecordStatusString $status_recordTime $bestMapRecordTime);
    ][
      return (getWeaponMapRecordStatusString $status_mapName $status_recordTime $status_weaponId);
    ]
  ]

]


// Private Functions

//
// Returns the record status (tied record, no new record) compared to the best map record.
// Must be called before the new record was saved.
//
// @param int $arg1 The record time in milliseconds
// @param int $arg2 The best map record time in milliseconds
//
// @return String The record status compared to the best map record
//
const getBestMapRecordStatusString [

  status_recordTime = $arg1;
  bestMapRecordTime = $arg2;

  if (= $status_recordTime $bestMapRecordTime)[
    return $messageTiedBestMapRecord;
  ][
    return $messageSlowerThanBestMapRecord;
  ]

]

//
// Returns the record status (new record, tied record, no new record) compared to the weapon map record.
// Must be called before the new record was saved.
//
// @param String $arg1 The map name
// @param int $arg2 The record time in milliseconds
// @param int $arg3 The weapon id of the weapon that the player used to finish the map
//
// @return String The record status compared to the weapon map record
//
const getWeaponMapRecordStatusString [

  status_mapName = $arg1;
  status_recordTime = $arg2;
  status_weaponId = $arg3;

  status_mapRecordTime = (loadMapRecord $status_mapName $status_weaponId);
  status_mapRecordWeapon = (getWeaponName $status_weaponId);

  if (|| (! $status_mapRecordTime) (< $status_recordTime $status_mapRecordTime)) [
    return (format $messageFasterThanWeaponMapRecord $status_mapRecordWeapon);
  ][
    if (= $status_recordTime $status_mapRecordTime) [
      return (format $messageTiedWeaponMapRecord $status_mapRecordWeapon);
    ][
      return (format $messageSlowerThanWeaponMapRecord $status_mapRecordWeapon);
    ]
  ]

]
