//
// @author wesen
// @copyright 2017-2019 wesen <wesen-ac@web.de>
//

//
// Map record module for ac-gema-mode
//
// Handles saving, loading and deleting of map records.
// The records are saved in a two dimensional array per map
// The structure of the array is [<weapon id>] => <best time>
//
// Requires:
//   * scripts/ac-gema-mode/util/array-utils.cfg
//

persistidents 0;

// Public Functions

//
// Saves a map record if it is better than the previous one.
//
// @param String $arg1 The map name
// @param int $arg2 The record time in milliseconds
// @param int $arg3 The weapon id
//
const saveMapRecord [

  saverecord_mapName = $arg1;
  saverecord_recordTime = $arg2;
  saverecord_weaponId = $arg3;

  saverecord_currentRecord = (loadMapRecord $saverecord_mapName $saverecord_weaponId);

  // If there is no record for this map or the record is worse than the new one 
  if (|| (! $saverecord_currentRecord) (< $saverecord_recordTime $saverecord_currentRecord)) [

    mapRecordListAlias = (getMapRecordListAlias $saverecord_mapName);
    if (! (isIdent $mapRecordListAlias)) [
      $mapRecordListAlias = "";
    ]

    mapRecordList = (getalias $mapRecordListAlias);
    mapRecordList = (array_set $mapRecordList (concat $saverecord_weaponId 0) $saverecord_recordTime);

    // save map records in saved.cfg
    persistidents 1;
    $mapRecordListAlias = $mapRecordList;
    persistidents 0;
  ]

]

//
// Loads a record and returns the map record time in milliseconds.
//
// @param String $arg1 The map name
// @param int $arg2 The weapon id
//
// @return int|string The map record time in milliseconds or an empty string if no reocrd was found
//
const loadMapRecord [

  mapName = $arg1;
  weaponId = $arg2;

  mapRecordListAlias = (getMapRecordListAlias $mapName);

  if (isIdent $mapRecordListAlias)[

    mapRecordList = (getalias $mapRecordListAlias);
    return (array_get $mapRecordList (concat $weaponId 0));

  ]

]

//
// Deletes a record.
//
// @param String $arg1 The map name
// @param String $arg2 The weapon id
//
const deleteMapRecord [

  mapName = $arg1;
  weaponId = $arg2;

  mapRecordListAlias = (getMapRecordListAlias $mapName);

  if (isIdent $mapRecordListAlias)[

    mapRecordList = (getalias $mapRecordListAlias);
    mapRecordList = (array_set $mapRecordList $weaponId "");

    persistidents 1;

    if (strcmp $mapRecordList "") [
      delalias $mapRecordListAlias;
    ][
      $mapRecordListAlias = $mapRecordList;
    ]

    persistidents 0;

    printGemaNotification $messageRecordDeleted;
  ]

]

//
// Deletes all map records for a specified map.
//
// @param string $arg1 The map name
//
const deleteAllMapRecords [

  mapName = $arg1;
  mapRecordListAlias = (getMapRecordListAlias $mapName);

  delalias $mapRecordListAlias;

]


// Private Functions

//
// Returns the map record list alias for a specific map.
//
// @param String $arg1 The map name
//
// @return String The map record list alias
//
const getMapRecordListAlias [
  return (concatword "::gema_records_" $mapName);
]
